<?php

require_once('data.php');

session_start();

$resultat = isset($_SESSION['resultat']) ? $_SESSION['resultat'] : NULL;
$erreur = isset($_SESSION['erreur']) ? $_SESSION['erreur'] : NULL;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Calcul d'itinéraires</title>
</head>
<body>

<form method="POST" action="itineraire.php">
        <select name="depart" placeholder="-- Ville de départ --">
            <option value="" disabled selected>-- Ville de départ --</option>
            <?php

// pour chaque ligne du tableau "nodes", lister une ville dans une case d'option
// jusqu'à ce que l'index soit inférieur ou égal à 12

$i = 0;
    while ($i <= 12) {
        foreach ($data as $villes) {
            echo "<option>" , $data['nodes'][$i] , "</option>" ;
            $i++;
        }
    }

?>
        </select>
        <select name="arrivee" placeholder="-- Ville d'arrivée --">
        <option value="" disabled selected>-- Ville d'arrivée --</option>  
            <option value=""> Midgard </option>
            <option value=""> Asgard </option>
            <option value=""> Vanaheim </option>
            <option value=""> Jotunheim </option>
            <option value=""> Niflheim </option>
            <option value=""> Muspelheim </option>
            <option value=""> Alfheim </option>
            <option value=""> Nidavellir </option>
            <option value=""> Hel </option>
            <option value=""> Odin's Pass </option>
            <option value=""> Ravenclaws </option>
            <option value=""> Shadow Steps </option>
            <option value=""> Iron Castle </option>
        </select>
        <button type="submit">Go !</button>
    </form>

<!--    Réalisez l'affichage de l'itinéraire calculé ici -->


</body>

